from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

class Role(db.Model):
    __tablename__='roles'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100), unique = True, nullable = False)

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
        }



class User(db.Model):
    __tablename__='users'
    id = db.Column(db.Integer, primary_key=True) #clave primaria
    nombre = db.Column(db.String(100), nullable=False)  #nullable=false -->que no puede estar nulo este campo
    apellido = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), unique=True) #unique=True --> que debe ser unico.
    password = db.Column(db.String(100), nullable=False)
    avatar = db.Column(db.String(250), nullable=True, default='default.jpg')#foto por default, mientras el usuario no suba una.
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False)
    role = db.relationship(Role)
    

    def serialize(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
            "apellido": self.apellido,
            "email":self.email,
    	    "avatar":self.avatar,
            "role": self.role.serialize()
        }